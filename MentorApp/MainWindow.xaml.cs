﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace MentorApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //c_Resume work_Resume = new c_Resume();
        c_JSON_reader work_JSON_reader = new c_JSON_reader();
        c_ResumeWork work_ResumeWork = new c_ResumeWork();

        public MainWindow()
        {
            InitializeComponent();

            work_ResumeWork.get_Professions();

            tb_Profession.ItemsSource = work_ResumeWork.profession_List;
        }

        private void b_test_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(resume_list.Count.ToString());
        }

        private void b_Show_Resume_Click(object sender, RoutedEventArgs e)
        {
            if (tb_Profession.Text != "" && tb_ExpMonth.Text != "")
            {
                dg_MatchResume.ItemsSource = null;
                dg_MatchResume.ItemsSource = work_ResumeWork.get_match_Professions(
                    work_ResumeWork.profession_List[tb_Profession.SelectedIndex].profession_tree_id,
                    tb_Best.Text
                );
            }
        }

        private void b_Show_NewResume_Click(object sender, RoutedEventArgs e)
        {
            //
        }

        private void DataGridRow_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = dg_MatchResume.ItemContainerGenerator.ContainerFromItem(dg_MatchResume.SelectedItem) as DataGridRow;
            c_ResumeWork.match_resume item = row.Item as c_ResumeWork.match_resume;

            //MessageBox.Show(item.best);

            dg_MatchResume2.ItemsSource = null;
            dg_MatchResume2.ItemsSource = work_ResumeWork.get_match_VacancyBest(item.best);
            //work_ResumeWork.get_match_ResumeBest();

            if (work_ResumeWork.best_3 > 0)
            {
                lb_Prof1.Content = work_ResumeWork.match_best_3_resume_List[0].desired_profession;
                lb_Best1.Text = work_ResumeWork.match_best_3_resume_List[0].candidat;
            }

            if (work_ResumeWork.best_3 > 1)
            {
                lb_Prof2.Content = work_ResumeWork.match_best_3_resume_List[1].desired_profession;
                lb_Best2.Text = work_ResumeWork.match_best_3_resume_List[1].candidat;
            }

            if (work_ResumeWork.best_3 > 2)
            {
                lb_Prof3.Content = work_ResumeWork.match_best_3_resume_List[2].desired_profession;
                lb_Best3.Text = work_ResumeWork.match_best_3_resume_List[2].candidat;
            }
        }
    }
}
