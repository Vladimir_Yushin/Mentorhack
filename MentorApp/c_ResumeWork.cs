﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows;

namespace MentorApp
{
    public class c_ResumeWork
    {
        public class profession
        {
            public string name { get; set; }
            public string profession_tree_id { get; set; }
            public string best { get; set; }
        }

        public class match_resume
        {
            public string desired_profession { get; set; }
            public string profession_tree_id { get; set; }
            public string best { get; set; }
            public string payment { get; set; }
            public string candidat { get; set; }
            public int c_match { get; set; }
        }

        public List<profession> profession_List = new List<profession> { };
        public List<profession> match_profession_List = new List<profession> { };
        public List<match_resume> match_resume_List = new List<match_resume> { };
        public List<match_resume> match_more_resume_List = new List<match_resume> { };
        public List<match_resume> match_best_resume_List = new List<match_resume> { };
        public List<match_resume> match_best_3_resume_List = new List<match_resume> { };
        List<string> best_positions = new List<string> { };

        public int best_3;

        public void get_Professions()
        {
            SqlConnection conn = new SqlConnection("Data Source=LAPTOP-M1JP2VRN\\SQLEXPRESS;Initial Catalog=MentorDB;User ID=sa;Password=12345");
            conn.Open();

            SqlCommand command = new SqlCommand("SELECT TOP 100 [profession_tree_name], [profession_tree_id] " +
                                                "FROM [resume] " +
                                                "GROUP BY [profession_tree_name], [profession_tree_id]", conn);

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    profession_List.Add(new profession
                    {
                        name = reader["profession_tree_name"].ToString(),
                        profession_tree_id = reader["profession_tree_id"].ToString(),
                    });

                }
            }
        }

        public List<match_resume> get_match_Professions(string profession_tree_id, string best)
        {
            match_resume_List.Clear();

            best_positions.Clear();
            List<string> matched_resume = new List<string> { };
            
            best_positions = get_parsed_List(best);

            SqlConnection conn = new SqlConnection("Data Source=LAPTOP-M1JP2VRN\\SQLEXPRESS;Initial Catalog=MentorDB;User ID=sa;Password=12345");
            conn.Open();

            SqlCommand command = new SqlCommand("SELECT [desired_profession], [best] " +
                                                "FROM [dbo].[resume] " +
                                                "WHERE profession_tree_id = " + profession_tree_id + " " +
                                                "GROUP BY [desired_profession], [best]", conn);

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    bool pass = false;

                    foreach (string item in best_positions)
                    {
                        matched_resume = get_parsed_List(reader["best"].ToString());

                        foreach (string resume_item in matched_resume)
                        {
                            if (item == resume_item)
                            {
                                pass = true;
                                //c_match++;
                            }
                        }
                    }

                    if (pass)
                    {
                        match_resume_List.Add(new match_resume
                        {
                            desired_profession = reader["desired_profession"].ToString(),
                            best = reader["best"].ToString(),
                        });
                    }
                }
            }

            conn.Close();

            return match_resume_List;
        }

        public List<match_resume> get_match_ProfessionsMonths(string profession_tree_id, string moths)
        {
            match_more_resume_List.Clear();

            SqlConnection conn = new SqlConnection("Data Source=LAPTOP-M1JP2VRN\\SQLEXPRESS;Initial Catalog=MentorDB;User ID=sa;Password=12345");
            conn.Open();

            SqlCommand command = new SqlCommand("SELECT [desired_profession] " +
                                                "FROM [dbo].[resume] " +
                                                "WHERE profession_tree_id = " + profession_tree_id + " and experience_month_total > " + moths + " " +
                                                "GROUP BY [desired_profession]", conn);

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    match_more_resume_List.Add(new match_resume
                    {
                        desired_profession = reader["desired_profession"].ToString()
                    });
                }
            }

            conn.Close();

            return match_more_resume_List;
        }

        List<string> get_parsed_List(string in_str)
        {
            List<string> best_positions = new List<string> { };

            int pos_delimeter = 1;
            string temp_str = in_str.Replace("Компьютерные навыки:", "");
            temp_str = temp_str.Trim();

            while (pos_delimeter != -1)
            {
                pos_delimeter = temp_str.IndexOf(",");

                if (pos_delimeter > -1)
                {
                    string fragment_str = temp_str.Substring(0, pos_delimeter);

                    best_positions.Add(fragment_str.Replace(".", ""));

                    temp_str = temp_str.Substring(pos_delimeter + 1, temp_str.Length - pos_delimeter - 1);
                }
                else
                {
                    best_positions.Add(temp_str.Replace(".", ""));
                }
            }

            return best_positions;
        }

        public List<match_resume> get_match_VacancyBest(string best)
        {
            match_best_resume_List.Clear();
            match_best_3_resume_List.Clear();

            List<string> best_positions_Resume = new List<string> { };
            List<string> vacancy_work_List = new List<string> { };
            List<string> vacancy_candidat_List = new List<string> { };

            best_3 = 0; //3 лучшие вакансии

            best_positions_Resume = get_parsed_List(best);

            SqlConnection conn = new SqlConnection("Data Source=LAPTOP-M1JP2VRN\\SQLEXPRESS;Initial Catalog=MentorDB;User ID=sa;Password=12345");
            conn.Open();

            SqlCommand command = new SqlCommand("SELECT [work], [candidat], [profession], [payment_from], [candidat] " +
                                                "FROM [dbo].[vacancy] " +
                                                "ORDER BY payment_from", conn);

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    bool pass = false;
                    int c_match = 0;

                    foreach (string item in best_positions_Resume)
                    {
                        vacancy_work_List = get_parsed_List(reader["work"].ToString());

                        foreach (string work_item in vacancy_work_List)
                        {
                            if (item == work_item)
                            {
                                pass = true;
                                c_match++;
                            }
                        }

                        vacancy_candidat_List = get_parsed_List(reader["candidat"].ToString());

                        foreach (string candidat_item in vacancy_candidat_List)
                        {
                            if (item == candidat_item)
                            {
                                pass = true;
                                c_match++;
                            }
                        }

                        /*if (reader["work"].ToString().IndexOf(item) > -1)
                        {
                            match_best_resume_List.Add(new match_resume { desired_profession = reader["profession"].ToString() });
                            break;
                        }

                        if (reader["candidat"].ToString().IndexOf(item) > -1)
                        {
                            match_best_resume_List.Add(new match_resume { desired_profession = reader["profession"].ToString() });
                            break;
                        }*/
                    }

                    if (pass)
                    {
                        if (best_3 < 3)
                        {
                            best_3++;

                            string parsed_candidat = reader["candidat"].ToString();

                            foreach (string item in best_positions)
                            {
                                if (item != "")
                                    parsed_candidat = parsed_candidat.Replace(item, "");
                            }

                            match_best_3_resume_List.Add(new match_resume
                            {
                                desired_profession = reader["profession"].ToString(),
                                payment = reader["payment_from"].ToString(),
                                candidat = parsed_candidat
                            });
                        }

                        match_best_resume_List.Add(new match_resume {
                            desired_profession = reader["profession"].ToString(),
                            payment = reader["payment_from"].ToString(),
                            candidat = reader["candidat"].ToString(),
                            c_match = c_match
                        });
                    }
                }
            }

            conn.Close();

            return match_best_resume_List;
        }

    }
}
