﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MentorApp
{
    class c_Resume
    {
        public class resume_data
        {
            public string town { get; set; }
            public string payment { get; set; }
            public string type_of_work { get; set; }
            public string place_of_work { get; set; }
            public string pol { get; set; }
            public int age { get; set; }
            public string maritalstatus { get; set; }
            public string children { get; set; }
            public string best { get; set; }
            public string dop { get; set; }
            public string driving_licence { get; set; }
            public bool business_trip { get; set; }
            public string citizenship { get; set; }
            public string desired_profession { get; set; }
            public int profession_tree_id { get; set; }
            public string profession_tree_name { get; set; }
            public int num_languages { get; set; }
            public int experience_month_total { get; set; }
            public List<string> catalogues { get; set; }

            //public List<catalogues_data> catalogues { get; set; }
            public List<work_history_data> work_history { get; set; }
            public List<base_education_data> base_education { get; set; }
            public List<additional_education_data> additional_education { get; set; }
            public List<language_data> languages { get; set; }
        }

        public class work_history_data
        {
            public string profession { get; set; }
            public string name { get; set; }
            public string company_scope { get; set; }
            public string work { get; set; }
            public string town { get; set; }
            public string type { get; set; }
            public string monthbeg { get; set; }
            public string yearbeg { get; set; }
            public string monthend { get; set; }
            public string yearend { get; set; }
            public string is_last { get; set; }
        }

        public class base_education_data
        {
            public string institute { get; set; }
            public string town { get; set; }
            public string monthend { get; set; }
            public string yearend { get; set; }
            public string faculty { get; set; }
            public string profession { get; set; }
            public string education_type { get; set; }
            public string education_form { get; set; }
        }

        public class additional_education_data
        {
            public string institute { get; set; }
            public string town { get; set; }
            public string name { get; set; }
            public string monthend { get; set; }
            public string yearend { get; set; }
        }

        public class language_data
        {
            public string name { get; set; }
            public string level { get; set; }
        }

        public List<resume_data> resume_data_list = new List<resume_data> { };
    }

    class c_Vacancy
    {
        public class vacancy_data
        {
            public string id_client { get; set; }
            public string town { get; set; }
            public string profession { get; set; }
            public string work { get; set; }
            public string candidat { get; set; }
            public string drive_license { get; set; }
            public string type_of_work_value { get; set; }
            public string place_of_work_value { get; set; }
            public string experience_value { get; set; }
            public string education_value { get; set; }
            public string employer_type_value { get; set; }
            public int payment_from { get; set; }
            public int payment_to { get; set; }
            public int profession_tree_id { get; set; }
            public string profession_tree_name { get; set; }

            public List<language_data> languages { get; set; }
            public List<metro_data> metro { get; set; }
        }

        public class language_data
        {
            public string name { get; set; }
            public string level { get; set; }
        }

        public class metro_data
        {
            public string station_id { get; set; }
            public string station_name { get; set; }
        }

        public List<vacancy_data> vacancy_data_list = new List<vacancy_data> { };
    }
}
