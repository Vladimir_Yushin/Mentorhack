﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Windows;
using System.Data.SqlClient;

namespace MentorApp
{
    public class c_JSON_reader
    {

        public void read_JSON_resume()
        {
            SqlConnection conn = new SqlConnection("Data Source=LAPTOP-M1JP2VRN\\SQLEXPRESS;Initial Catalog=MentorDB;User ID=sa;Password=12345");

            int i_record = 0;

            StreamReader r = new StreamReader(@"D:\resumes.json");

            while (r.Peek() >= 0)
            {
                i_record++;
                //List<resume> items = JsonConvert.DeserializeObject<List<resume>>(parse_str);

                string JSON = r.ReadLine();
                byte[] byteArray = Encoding.UTF8.GetBytes(JSON);
                //byte[] byteArray = Encoding.ASCII.GetBytes(contents);
                MemoryStream stream = new MemoryStream(byteArray);
                // convert stream to string

                JsonSerializer se = new JsonSerializer();

                StreamReader re = new StreamReader(stream);
                JsonTextReader reader = new JsonTextReader(re);
                var DeserializedObject = se.Deserialize<c_Resume.resume_data>(reader);

                string town = parse_String(DeserializedObject.town);
                string payment = parse_String(DeserializedObject.payment);
                string type_of_work = parse_String(DeserializedObject.type_of_work);
                string place_of_work = parse_String(DeserializedObject.place_of_work);
                string pol = parse_String(DeserializedObject.pol);
                int age = DeserializedObject.age;
                string maritalstatus = parse_String(DeserializedObject.maritalstatus);
                string children = parse_String(DeserializedObject.children);
                string best = parse_String(DeserializedObject.best);
                string dop = parse_String(DeserializedObject.dop);
                string driving_licence = parse_String(DeserializedObject.driving_licence);
                bool business_trip = bool.Parse(DeserializedObject.business_trip.ToString());
                string citizenship = parse_String(DeserializedObject.citizenship);
                string desired_profession = parse_String(DeserializedObject.desired_profession);
                int profession_tree_id = DeserializedObject.profession_tree_id;
                string profession_tree_name = parse_String(DeserializedObject.profession_tree_name);
                int num_languages = DeserializedObject.num_languages;
                int experience_month_total = DeserializedObject.experience_month_total;

                //MessageBox.Show(business_trip.ToString());

                conn.Open();

                SqlCommand command = new SqlCommand("INSERT [resume] " +
                                                    "([best],[age],[town],[payment],[type_of_work],[place_of_work]," +
                                                    "[pol],[maritalstatus],[children],[dop],[driving_licence],[business_trip]," +
                                                    "[citizenship],[desired_profession],[profession_tree_id],[profession_tree_name]," +
                                                    "[num_languages],[experience_month_total]) " +
                                                    "VALUES ('" + best + "'," + age + ",'" + town + "','" + payment + "'," +
                                                    "'" + type_of_work + "','" + place_of_work + "','" + pol + "','" + maritalstatus + "'," +
                                                    "'" + children + "','" + dop + "','" + driving_licence + "','" + business_trip + "'," +
                                                    "'" + citizenship + "','" + desired_profession + "'," + profession_tree_id + ",'" + profession_tree_name + "'," +
                                                    "" + num_languages + "," + experience_month_total + ")", conn);

                command.ExecuteNonQuery();

                conn.Close();

                foreach (string item in DeserializedObject.catalogues)
                {
                    conn.Open();

                    command.CommandText = "INSERT [catalogues] " +
                                          "([id_resume], [name]) " +
                                          "VALUES (" + i_record + ", '" + item + "')";

                    command.ExecuteNonQuery();

                    conn.Close();
                }

                foreach (c_Resume.work_history_data item in DeserializedObject.work_history)
                {
                    string profession = parse_String(item.profession);
                    string name = parse_String(item.name);
                    string company_scope = parse_String(item.company_scope);
                    string work = parse_String(item.work);
                    string town_wh = parse_String(item.town);
                    string type = parse_String(item.type);
                    string monthbeg = parse_String(item.monthbeg);
                    string yearbeg = parse_String(item.yearbeg);
                    string monthend = parse_String(item.monthend);
                    string yearend = parse_String(item.yearend);
                    string is_last = parse_String(item.is_last);

                    conn.Open();

                    command.CommandText = "INSERT [work_history] " +
                                          "([id_resume],[profession],[name],[company_scope],[work],[town],[type]," +
                                          "[monthbeg],[yearbeg],[monthend],[yearend],[is_last]) " +
                                          "VALUES (" + i_record + ", '" + profession + "', '" + name + "', " +
                                          "'" + company_scope + "', '" + work + "', '" + town_wh + "', " +
                                          "'" + type + "', '" + monthbeg + "', '" + yearbeg + "', " +
                                          "'" + monthend + "', '" + yearend + "', '" + is_last + "')";

                    command.ExecuteNonQuery();

                    conn.Close();
                }

                foreach (c_Resume.base_education_data item in DeserializedObject.base_education)
                {
                    string institute = parse_String(item.institute);
                    string town_ed = parse_String(item.town);
                    string monthend = parse_String(item.monthend);
                    string yearend = parse_String(item.yearend);
                    string faculty = parse_String(item.faculty);
                    string profession = parse_String(item.profession);
                    string education_type = parse_String(item.education_type);
                    string education_form = parse_String(item.education_form);

                    conn.Open();

                    command.CommandText = "INSERT [base_education] " +
                                          "([id_resume],[institute],[town],[monthend],[yearend],[faculty]," +
                                          "[profession]," +
                                          "[education_type],[education_form]) " +
                                          "VALUES (" + i_record + ", '" + institute + "', '" + town_ed + "'," +
                                          "'" + monthend + "', '" + yearend + "', '" + faculty + "'," +
                                          "'" + profession + "', '" + education_type + "', '" + education_form + "')";

                    command.ExecuteNonQuery();

                    conn.Close();
                }

                foreach (c_Resume.additional_education_data item in DeserializedObject.additional_education)
                {
                    string institute = parse_String(item.institute);
                    string town_ade = parse_String(item.town);
                    string name = parse_String(item.name);
                    string monthend = parse_String(item.monthend);
                    string yearend = parse_String(item.yearend);

                    conn.Open();

                    command.CommandText = "INSERT [additional_education] " +
                                          "([id_resume],[institute],[town],[name],[monthend],[yearend]) " +
                                          "VALUES (" + i_record + ", '" + institute + "', '" + town_ade + "'," +
                                          "'" + name + "', '" + monthend + "','" + yearend + "')";

                    command.ExecuteNonQuery();

                    conn.Close();
                }

                foreach (c_Resume.language_data item in DeserializedObject.languages)
                {
                    string name = parse_String(item.name);
                    string level = parse_String(item.level);

                    conn.Open();

                    command.CommandText = "INSERT [languages] " +
                                          "([id_resume],[name],[level]) " +
                                          "VALUES (" + i_record + ", '" + name + "', '" + level + "')";

                    command.ExecuteNonQuery();

                    conn.Close();
                }

                //MessageBox.Show(DeserializedObject.profession_tree_id.ToString());

                //if (DeserializedObject.catalogues.Count > 0)
                //MessageBox.Show(DeserializedObject.catalogues[0]);

                /*
                //MessageBox.Show(parse_str);
                if (parse_str.Contains(@"payment"))
                {
                    //string substring = parse_str.Substring(parse_str.IndexOf("id") + 10, 200);
                    string substring = parse_str.Substring(parse_str.IndexOf("id") + 5, 200);

                    //MessageBox.Show(parse_str.Substring(parse_str.IndexOf("id") + 5, substring.IndexOf(",") - 1));
                    //MessageBox.Show(parse_str.Substring(parse_str.IndexOf("payment") + 10, substring.IndexOf(",") - 1));
                }*/

                
            }
        }

        public void read_JSON_vacancy()
        {
            SqlConnection conn = new SqlConnection("Data Source=LAPTOP-M1JP2VRN\\SQLEXPRESS;Initial Catalog=MentorDB;User ID=sa;Password=12345");

            int i_record = 0;

            StreamReader r = new StreamReader(@"D:\vacancy_data_190.json");

            while (r.Peek() >= 0)
            {
                i_record++;
                //List<resume> items = JsonConvert.DeserializeObject<List<resume>>(parse_str);

                string JSON = r.ReadLine();
                byte[] byteArray = Encoding.UTF8.GetBytes(JSON);
                //byte[] byteArray = Encoding.ASCII.GetBytes(contents);
                MemoryStream stream = new MemoryStream(byteArray);
                // convert stream to string

                JsonSerializer se = new JsonSerializer();

                StreamReader re = new StreamReader(stream);
                JsonTextReader reader = new JsonTextReader(re);
                var DeserializedObject = se.Deserialize<c_Vacancy.vacancy_data>(reader);

                string id_client = parse_String(DeserializedObject.id_client);
                string town = parse_String(DeserializedObject.town);
                string profession = parse_String(DeserializedObject.profession);
                string work = parse_String(DeserializedObject.work);
                string candidat = parse_String(DeserializedObject.candidat);
                string drive_license = parse_String(DeserializedObject.drive_license);
                string type_of_work_value = parse_String(DeserializedObject.type_of_work_value);
                string place_of_work_value = parse_String(DeserializedObject.place_of_work_value);
                string experience_value = parse_String(DeserializedObject.experience_value);
                string education_value = parse_String(DeserializedObject.education_value);
                string employer_type_value = parse_String(DeserializedObject.employer_type_value);
                int payment_from = DeserializedObject.payment_from;
                int payment_to = DeserializedObject.payment_to;
                int profession_tree_id = DeserializedObject.profession_tree_id;
                string profession_tree_name = parse_String(DeserializedObject.profession_tree_name);

                conn.Open();

                SqlCommand command = new SqlCommand("INSERT [vacancy] " +
                                                    "([id_client],[town],[profession],[work],[candidat],[drive_license]," +
                                                    "[type_of_work_value],[place_of_work_value],[experience_value],[education_value]," +
                                                    "[employer_type_value],[payment_from],[payment_to],[profession_tree_id],[profession_tree_name]) " +
                                                    "VALUES ('" + id_client + "','" + town + "','" + profession + "','" + work + "'," +
                                                    "'" + candidat + "','" + drive_license + "','" + type_of_work_value + "','" + place_of_work_value + "'," +
                                                    "'" + experience_value + "','" + education_value + "','" + employer_type_value + "'," + payment_from + "," +
                                                    "" + payment_to + "," + profession_tree_id + ",'" + profession_tree_name + "')", conn);

                command.ExecuteNonQuery();

                conn.Close();

                foreach (c_Vacancy.metro_data item in DeserializedObject.metro)
                {
                    string station_id = item.station_id;
                    string station_name = parse_String(item.station_name);

                    conn.Open();

                    command.CommandText = "INSERT [metro] " +
                                          "([id_vacancy],[station_id],[station_name]) " +
                                          "VALUES (" + i_record + ", " + station_id + ", '" + station_name + "')";

                    command.ExecuteNonQuery();

                    conn.Close();
                }

                foreach (c_Vacancy.language_data item in DeserializedObject.languages)
                {
                    string name = parse_String(item.name);
                    string level = parse_String(item.level);

                    conn.Open();

                    command.CommandText = "INSERT [languages_vacancy] " +
                                          "([id_vacancy],[name],[level]) " +
                                          "VALUES (" + i_record + ", '" + name + "', '" + level + "')";

                    command.ExecuteNonQuery();

                    conn.Close();
                }

                //MessageBox.Show(DeserializedObject.place_of_work_value.ToString());
            }
        }

        string parse_String(string in_str)
        {
            if (in_str != null)
                return in_str.Replace("\'", "''");
            else
                return "";
        }
    }
}
